import styled from 'styled-components/native'

export default styled.View`
  padding: 25px;
  flex: 1;
  background: #fff;
`;