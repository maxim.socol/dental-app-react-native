import React from 'react';
import styled from 'styled-components/native';

const getColor = ({ active, color }) => {
  const colors = {
    green: {
      background: '#70c7be',
      color: '#ffffff',
    },
    active: {
      background: '#6f95ff',
      color: '#ffffff'
    },
    default: {
      background: '#70c7be',
      color: '#ffffff'
    }
  };

  let result;
  if (active) {
    result = colors.active;
  } else if (color && colors[color]) {
    result = colors[color];
  } else {
    result = colors.default;
  }

  return result;
};

export default styled.Text`
  background: ${props => getColor(props).background};
  color: ${props => getColor(props).color};
  font-weight: 600;
  font-size: 14px;
  width: 70px;
  height: 32px;
  text-align: center;
  line-height: 32px;
`;