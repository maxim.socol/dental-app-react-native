import React from 'react';
import styled from 'styled-components/native'

const Button = ({ children, color, onPress }) => (
    <ButtonWrapper onPress={onPress} color={color}>
        <ButtonText>{ children }</ButtonText>
    </ButtonWrapper>
);

Button.defaultProps = {
    color: '#6f95ff',  
};

const ButtonWrapper = styled.TouchableOpacity`
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 30px;
    background: ${props => props.color};
    text-align: center;
    height: 45px;
`;

const ButtonText = styled.Text`
    color: #ffffff;
    font-weight: 500;
    font-size: 16px;
`;

export default Button;