import React from 'react';
import styled from 'styled-components/native';
import { Ionicons } from '@expo/vector-icons';

const PlusButton = ({ onPress }) => (
    <Circle style={{
        shadowColor: '#6f95ff',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.5,
        elevation: 10,
    }} onPress={onPress}>
        <Ionicons name="ios-add" size={36} color="white" />
    </Circle>
);

const Circle = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  border-radius: 50px;
  width: 64px;
  height: 64px;
  line-height: 64px;
  background-color: #6f95ff;
  position: absolute;
  right: 20px;
  bottom: 25px;
`;

export default PlusButton;