import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StartScreen, HomeScreen, PatientScreen, AddPatientScreen, AddAppointmentScreen, PatientsScreen } from "./screens";
import { Button } from 'native-base';
import { Fontisto, Ionicons } from '@expo/vector-icons'; 

const Stack = createStackNavigator();
console.disableYellowBox = true;

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Start" component={ StartScreen } options={{
          headerTintColor: '#fff',
          headerStyle: { 
            backgroundColor: '#fff',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            },
          }
        }} />
        <Stack.Screen name="Home" component={HomeScreen} options={({ navigation, route }) => ({
          headerLeft: null,
          headerRight: () => (
            <TouchableOpacity
              onPress={navigation.navigate.bind(this, 'Patients')}
              style={{ marginRight: 20 }}
            >
              <Ionicons name="md-people" size={28} color="#6f95ff" />
            </TouchableOpacity>
          ),
          title: 'Журнал приёмов', 
          headerTintColor: '#403D42',
          headerTitleAlign: 'left',
          headerStyle: {
            elevation: 0.8,
            shadowOpacity: 0.8,
          },
          headerTitleStyle: {
            fontSize: 22,
            fontWeight: '700',
            marginBottom: 10
          },
          headerLayoutPreset: 'center'
        })} />
        <Stack.Screen name="Patient" component={PatientScreen} options={({ navigation, route }) => ({
          headerLeft: ({ props }) => (
            <Text style={{ marginLeft: 10 }} {...props} onPress={() => { navigation.navigate("Home") }}>
              <Fontisto name="arrow-left-l" size={24} color="#6f95ff" />
            </Text>
          ),
          title: 'Карта пациента', 
          headerTintColor: '#403D42',
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0.8,
            shadowOpacity: 0.8,
          },
          headerTitleStyle: {
            fontSize: 22,
            fontWeight: '700',
            marginBottom: 10
          },
        })} />
        <Stack.Screen name="Patients" component={PatientsScreen} options={({ navigation, route }) => ({
          headerLeft: ({ props }) => (
            <Text style={{ marginLeft: 10 }} {...props} onPress={() => { navigation.navigate("Home") }}>
              <Fontisto name="arrow-left-l" size={24} color="#6f95ff" />
            </Text>
          ),
          title: 'Пациенты', 
          headerTintColor: '#403D42',
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0.8,
            shadowOpacity: 0.8,
          },
          headerTitleStyle: {
            fontSize: 22,
            fontWeight: '700',
            marginBottom: 10
          },
        })} />
        <Stack.Screen name="AddPatient" component={AddPatientScreen} options={({ navigation, route }) => ({
          headerLeft: ({ props }) => (
            <Text style={{ marginLeft: 10 }} {...props} onPress={() => { navigation.navigate("Home") }}>
              <Fontisto name="arrow-left-l" size={24} color="#6f95ff" />
            </Text>
          ),
          title: 'Добавить пациента', 
          headerTintColor: '#403D42',
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0.8,
            shadowOpacity: 0.8,
          },
          headerTitleStyle: {
            fontSize: 22,
            fontWeight: '700',
            marginBottom: 10
          },
        })} />
        <Stack.Screen name="AddAppointment" component={AddAppointmentScreen} options={({ navigation, route }) => ({
          headerLeft: ({ props }) => (
            <Text style={{ marginLeft: 10 }} {...props} onPress={() => { navigation.navigate("Patient") }}>
              <Fontisto name="arrow-left-l" size={24} color="#6f95ff" />
            </Text>
          ),
          title: 'Добавить приём',
          headerTintColor: '#403D42',
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0.8,
            shadowOpacity: 0.8,
          },
          headerTitleStyle: {
            fontSize: 22,
            fontWeight: '700',
            marginBottom: 10
          },
        })} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
