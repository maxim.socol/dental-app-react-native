import React, { useState } from 'react';
import { View, Text, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Item, Input, Label, Picker } from 'native-base';
import styled from 'styled-components/native';
import DatePicker from 'react-native-datepicker';

import { appointmentsApi } from '../utils/api';

import { Button, Container } from '../components';

const AddAppointmentScreen = (props) => {
    const { navigation, route } = props;

    const [values, setValues] = useState({
        diagnosis: '',
        dentNumber: '',
        price: '',
        date: null,
        time: null,
        patient: route.params.patientId
    });

    const fieldsName = {
        diagnosis: 'Диагноз',
        dentNumber: 'Номер зуба',
        price: 'Цена',
        date: 'Дата',
        time: 'Время'
    };

    const setFieldValue = (name, value) => {
        setValues({
            ...values,
            [name]: value
        });
    };

    const handleInputChange = (name, e) => {
        const text = e.nativeEvent.text;
        setFieldValue(name, text);
    };

    const onSubmit = () => {
        appointmentsApi
        .add(values)
        .then(() => {
            navigation.navigate('Home', { lastUpdate: new Date() });
        })
        .catch(e => {
            if (e.response.data && e.response.data.message) {
                e.response.data.message.forEach(err => {
                    const fieldName = err.param;
                    Alert.alert(
                        'Ошибка',
                        `Поле "${fieldsName[fieldName]}" указано неверно.`
                      );
                });
            }
        });
    };

    return (
        <Container>
            <Item style={{ marginLeft: 0 }}>
                <Picker
                    mode="dropdown"
                    placeholder="Выберите диагноз"
                    placeholderStyle={{ color: '#333333', fontSize: 17, paddingLeft: 0, marginBottom: 12 }}
                    placeholderIconColor="#007aff"
                    style={{ width: '100%' }}
                    onValueChange={setFieldValue.bind(this, 'diagnosis')}
                    selectedValue={values.diagnosis}
                >
                    <Picker.Item label="пульпит" value="пульпит" />
                    <Picker.Item label="удаление зуба" value="удаление зуба" />
                    <Picker.Item label="отбеливание" value="отбеливание" />
                    <Picker.Item label="удаление нерва" value="удаление нерва" />
                </Picker>
            </Item>
            <Item style={{ marginTop: 20, marginLeft: 0 }} floatingLabel>
                <Label>Номер зуба</Label>
                <Input
                    onChange={handleInputChange.bind(this, 'dentNumber')}
                    value={values.dentNumber}
                    style={{ marginTop: 12 }}
                    keyboardType="numeric"
                />
            </Item>
            <Item style={{ marginTop: 20, marginLeft: 0 }} floatingLabel>
                <Label>Цена (MDL)</Label>
                <Input
                    onChange={handleInputChange.bind(this, 'price')}
                    value={values.price}
                    style={{ marginTop: 12 }}
                    keyboardType="numeric"
                />
            </Item>
            <Item style={{ marginTop: 20, marginLeft: 0 }}>
                <TimeRow>
                    <View style={{ flex: 1 }}>
                        <DatePicker
                            date={new Date()}
                            mode="date"
                            placeholder="Дата"
                            format="YYYY-MM-DD"
                            minDate={new Date()}
                            confirmBtnText="Сохранить"
                            cancelBtnText="Отмена"
                            showIcon={false}
                            customStyles={{
                                dateInput: { borderWidth: 0, },
                                dateText: { fontSize: 18 }
                            }}
                            date={values.date}
                            onDateChange={setFieldValue.bind(this, 'date')}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <DatePicker
                            mode="time"
                            placeholder="Время"
                            format="HH:mm"
                            minDate={new Date()}
                            confirmBtnText="Сохранить"
                            cancelBtnText="Отмена"
                            showIcon={false}
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0
                                },
                                dateText: {
                                    fontSize: 18
                                }
                            }}
                            date={values.time}
                            onDateChange={setFieldValue.bind(this, 'time')}
                        />
                    </View>
                </TimeRow>
            </Item>
            <ButtonView>
                <Button onPress={onSubmit} color="#6f95ff">
                    {/* <Ionicons style={{ lineHeight: 47 }} name="ios-add" size={24} color="white" /> */}
                    <Text>Добавить приём</Text>
                </Button>
            </ButtonView>
        </Container>
    );
};

const TimeRow = styled.View`
  flex-direction: row;
`;

const ButtonView = styled.View`
  flex: 1;
  margin-top: 30px;
`;

export default AddAppointmentScreen;