import React from 'react';
import { Text, Image, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';

const StartScreen = props => {
const { navigation } = props;
    return (
        <Container>
            <View style={styles.section}>
                <Image style={styles.logo} source={require('../assets/logo.png')} />
                <Text style={styles.title}>Welcome to Dr. Dentist Support</Text>
                <Text style={styles.text}>Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus, quis mollis nisl</Text>
            </View>
            <ButtonView>
                <Button onPress={navigation.navigate.bind(this, 'Home')}>
                    <Text style={{color: '#fff', fontSize: '18px', fontWeight: '700' }}>Продолжить</Text>
                </Button>
            </ButtonView>
        </Container>
    )
};

const ButtonView = styled.View`
  flex: 1;
`;

const styles = StyleSheet.create({
    section: {
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 50
    },
    logo: {
        alignSelf: 'center',
        marginBottom: 100
    },
    title: {
        fontSize: 30,
        textAlign: 'center',
        alignSelf: 'center',
        marginBottom: 15
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
        alignSelf: 'center',
    }
});

const Button = styled.TouchableOpacity`
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 30px;
    background: #6f95ff;
    text-align: center;
    height: 60px;
    bottom: 150px;
    width: 100%;
`;

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  padding: 25px;
`;

export default StartScreen;