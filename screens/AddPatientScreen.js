import React, { useState } from 'react';
import { Text, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Item, Input, Label, Picker } from 'native-base';
import styled from 'styled-components/native';

import { patientsApi } from '../utils/api';

import { Button, Container } from '../components';

const AddPatientScreen = (props) => {
  const { navigation, route } = props;

  const [values, setValues] = useState({
    fullname: '',
    phone: ''
  });

const fieldsName = {
    fullname: 'Имя и Фамилия',
    phone: 'Номер телефона',
  };

  const setFieldValue = (name, value) => {
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleChange = (name, e) => {
    const text = e.nativeEvent.text;
    setFieldValue(name, text);
  };

  const onSubmit = () => {
    patientsApi
    .add(values)
    .then(() => {
      navigation.navigate('Home');
    })
    .catch(e => {
      if (e.response.data && e.response.data.message) {
        e.response.data.message.forEach(err => {
            const fieldName = err.param;
            Alert.alert(
              'Ошибка',
              `Поле "${fieldsName[fieldName]}" указано неверно.`
            );
        });
      }
    });
  };

  return (
    <Container>
      <Item style={{ marginLeft: 0 }} floatingLabel>
        <Label>Имя и Фамилия</Label>
        <Input
          onChange={handleChange.bind(this, 'fullname')}
          value={ values.fullname }
          style={{ marginTop: 12 }}
          autoFocus
        />
      </Item>
      <Item style={{ marginTop: 20, marginLeft: 0 }} floatingLabel>
        <Label>Номер телефона</Label>
        <Input
          onChange={handleChange.bind(this, 'phone')}
          value={ values.phone }
          keyboardType="numeric"
          dataDetectorTypes="phoneNumber"
          style={{ marginTop: 12 }}
        />
      </Item>
      <Item style={{ marginTop: 35, marginLeft: 0 }}>
        <Picker
          mode="dropdown"
          placeholder="Выберите пол"
          placeholderStyle={{ color: '#333333', fontSize: 17, paddingLeft: 0, marginBottom: 12 }}
          placeholderIconColor="#007aff"
          style={{ width: '100%' }}
          onValueChange={setFieldValue.bind(this, 'sex')}
          selectedValue={ values.sex }
        >
          <Picker.Item label="М" value="М" />
          <Picker.Item label="Ж" value="Ж" />
        </Picker>
      </Item>
      <ButtonView>
        <Button onPress={onSubmit} color="#6f95ff">
          {/* <Ionicons style={{ lineHeight: 47 }} name="ios-add" size={24} color="white" /> */}
          <Text>Добавить пациента</Text>
        </Button>
      </ButtonView>
    </Container>
  );
};

const ButtonView = styled.View`
  flex: 1;
  margin-top: 30px;
`;

export default AddPatientScreen;