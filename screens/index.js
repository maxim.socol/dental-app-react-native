export { default as StartScreen } from './StartScreen';
export { default as HomeScreen } from './HomeScreen';
export { default as PatientScreen } from './PatientScreen';
export { default as PatientsScreen } from './PatientsScreen';
export { default as AddPatientScreen } from './AddPatientScreen';
export { default as AddAppointmentScreen } from './AddAppointmentScreen';