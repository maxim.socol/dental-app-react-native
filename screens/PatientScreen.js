import React, { useEffect, useState } from 'react';
import { Text, View, ActivityIndicator, Linking } from 'react-native';
import styled from 'styled-components/native';
import { Foundation, Ionicons, FontAwesome5 } from '@expo/vector-icons';

import { GrayText, Button, Badge, Container, Appointment, PlusButton } from '../components';
import { patientsApi, phoneFormat } from '../utils';

const PatientScreen = (props) => {
    const { navigation, route } = props;
    const params = route.params;
    const [appointments, setAppointments] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        // const id = navigation.getParam('patient')._id;
        const id = route.params.patient._id;
        // console.log(route.params.patient._id);

        patientsApi.show(id).then(({ data }) => {
            // console.log(data.data);
            setAppointments(data.data);
            setIsLoading(false);
        })
        .catch(() => {
            setIsLoading(false);
        });
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <PatientDetails>
                <PatientFullname>{ params.patient.fullname }</PatientFullname>
                <GrayText>{ phoneFormat(params.patient.phone) }</GrayText>
                <PatientButtons>
                    <FormulaButtonView>
                        <Button color="#6f95ff">Формула зубов</Button>
                    </FormulaButtonView>
                    <PhoneButtonView>
                        <Button onPress={() => Linking.openURL('tel:' + params.patient.phone)} color="#9F6893">
                            <Foundation name="telephone" size={22} color="white" />
                        </Button>
                    </PhoneButtonView>
                </PatientButtons>
            </PatientDetails>

            <PatientAppointments>
                <Container>
                    {isLoading ? (
                        <ActivityIndicator size="large" color="#6f95ff" />
                    ) : (
                    appointments.map(appointment => (
                        <AppointmentCard key={ appointment._id } style={{
                            shadowColor: 'gray',
                            shadowOpacity: 0.1,
                            shadowRadius: 5,
                            elevation: 0.5,
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                        }}>
                            <MoreButton>
                                <Ionicons name="md-more" size={24} color="rgba(0, 0, 0, 0.4)" />
                            </MoreButton>
                            <AppointmentCardRow>
                                <FontAwesome5 name="tooth" size={18} color="#A3A3A3" />
                                <AppointmentCardLabel>
                                    Зуб: <Text style={{ fontWeight: '600' }}>{ route.params.dentNumber }</Text>
                                </AppointmentCardLabel>
                            </AppointmentCardRow>
                            <AppointmentCardRow>
                                <FontAwesome5 name="clipboard-list" size={18} color="#A3A3A3" />
                                <AppointmentCardLabel>
                                    Диагноз: <Text style={{ fontWeight: '600' }}>{ route.params.diagnosis }</Text>
                                </AppointmentCardLabel>
                            </AppointmentCardRow>
                            <AppointmentCardRow style={{ marginTop: 15, justifyContent: 'space-between' }}>
                                <Badge style={{ width: 155, borderRadius: 16, overflow: "hidden" }} active>
                                    { route.params.date } - { route.params.time }
                                </Badge>
                                <Badge color="green" style={{ width: 85, borderRadius: 16, overflow: "hidden" }}>{ route.params.price } MDL</Badge>
                            </AppointmentCardRow>
                        </AppointmentCard>))
                    )}
                </Container>
            </PatientAppointments>
            <PlusButton onPress={navigation.navigate.bind(this, 'AddAppointment', { patientId: route.params.patient._id })} />
        </View>
    );
};

const MoreButton = styled.TouchableOpacity`
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 10px;
    top: 10px;
    height: 32px;
    width: 32px;
`;

const AppointmentCardLabel = styled.Text`
    font-size: 16px;
    margin-left: 10px;
`;

const AppointmentCardRow = styled.View`
    flex-direction: row;
    align-items: center;
    margin-top: 3.5px;
    margin-bottom: 3.5px;
`;

const AppointmentCard = styled.View`
    padding: 20px 25px;
    border-radius: 10px;
    background: white;
    margin-bottom: 20px;
`;

const PatientDetails = styled(Container)`
    flex: 0.2; 
    background-color: #ffffff;
`;

const PatientAppointments = styled.View`
    flex: 1;
    background: #f8fafd;
`;

const FormulaButtonView = styled.View`
    flex: 1;
`;

const PhoneButtonView = styled.View`
    width: 45px;
    margin-left: 10px;
`;

const FormulaButton = styled(Button)`
    align-self: stretch;
`;

const PhoneButton = styled(Button)`
    background: #84D269;
    width: 45px;
    height: 45px;
`;

const PatientButtons = styled.View`
    flex: 1;
    flex-direction: row;
    margin-top: 20px;
`;

const PatientFullname = styled.Text`
    font-weight: 800;
    font-size: 24px;
    line-height: 30px;
    margin-bottom: 5px;
`;

export default PatientScreen;